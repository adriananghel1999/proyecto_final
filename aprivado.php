<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Proyecto 1DAW - SOLVAM</title>
    
    <!-- Fuentes ================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Ranga" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
	<!-- CSS ================================================== -->
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/estructura.css">
    <link rel="stylesheet" href="css/logoymenu.css">
    <link rel="stylesheet" href="css/textoprin.css">
    <link rel="stylesheet" href="css/pie.css">   
	<!-- Favicons ================================================== -->
	<link rel="shortcut icon" href="img/favicon.ico">

	<!-- JS ================================================== -->
  <script src="js/jquery-3.2.0.min.js"></script>
  <script src="js/index.js"></script>

</head>

<body>
    <div id="contenedor">
        <div id="encabezado">
            <div id="menulogo">
               <div id="logo">
                   <img src="img/logo.png" />
                   <p>MYTHOLOGY <span>Articulos robados de wikipedia</span></p>
               </div>
               <div id="menu">
                   <ul>
                       <li><a href="contacto.php">Contacto</a></li>
                       <li><a href="aprivado.php">Acceso Privado</a></li>
                       <li><a href="blog.php">Blog<span class="flecha"></span></a>
                           <ul class="submenu">
                              
                            <?php include ("./php/submenu.php");?>
                               
                           </ul>
                      </li>
                       <li><a href="index.php">Inicio</a></li>
                   </ul>
               </div>
            </div>
            
        </div>
        
        <div class="limpiar"></div>

        <div id="framejsp">
   <iframe align="middle" width="1170px" height="1000px" scrolling="yes" frameborder="0" src="http://ruman.com.es:8080/ruman/acceso/login.jsp" alt="Banner">
   </iframe>
            </div>

        <div id="pie">
            <div id="pie1">
                <p>Sobre Nosotros</p>
                <p><img src="img/logo-peque.png"></p>
                <p class="direccion">C/ Cervantes, 3 <br />Quart de Poblet - 46930 <br />Valencia<br /><br /> Tel. 96 154 77 93</p>
                <div class="social">
                    <a href="http://www.facebook.com/pages/Solvam/333168923387556" target="_blank"><span class="face"></span></a>
                    <a href="https://twitter.com/#!/solvam" target="_blank"><span class="twit"></span></a>
                    <a href="https://plus.google.com/109456103380274611539/posts?hl=es"  target="_blank"><span class="goog"></span></a>
                    <a href="#"><span class="rss"></span></a>
                    <a href="https://www.instagram.com/centrofpsolvam/"  target="_blank"><span class="inst"></span></a>
                </div>
            </div>
            <div id="pie2">
                <p>Donde estamos</p>
                <div class="mapa">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3079.404071873097!2d-0.4425859499951611!3d39.48278957938388!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd604ff779223b39%3A0xc60596edf6e35ebd!2sCentro+de+FP+SOLVAM!5e0!3m2!1ses!2ses!4v1490268805510" width="240" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div id="pie3">
                <p>Últimos posts</p>
                <ul>
                    <?php include ("./php/pie.php");?>
                </ul>
            </div>
            <div id="pie4">
                <p>Nuestros vídeos</p>
                <div class="video">
                    <iframe width="240" height="200" src="https://www.youtube.com/embed/BsWN6p_85zs" frameborder="0" allowfullscreen></iframe>

                </div>
            </div>
            <div class="limpiar"></div>
            <div id="pie5">
                <p class="izq" id="copyright">Copyright 2017 SOLVAM - Todos los derechos reservados</p>

                <p class="der"><a href="index.php">Inicio</a> | <a href="blog.php">Blog</a> | <a href="aprivado.php">Acceso Privado</a> | <a href="contacto.php">Contacto</a></p>
            </div>
            <div class="arriba"></div>
            <div class="limpiar"></div>
        </div>
    </div>
</body>
</html>





















