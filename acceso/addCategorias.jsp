<%@ page import="java.sql.*" %>  

<%
try
{	
	Class.forName("com.mysql.jdbc.Driver");  //driver 
	
	Connection con=DriverManager.getConnection("jdbc:mysql://url","user","pass"); //create connection 

	if(request.getParameter("btn_add")!=null) //click del boton no null
	{
		String categoria;
		
		categoria=request.getParameter("txt_categoria"); //categoria
		
		PreparedStatement pstmt=null; // statement 
		
		pstmt=con.prepareStatement("insert into categorias(categoria)values(?)"); //sql query 
		pstmt.setString(1,categoria);
		pstmt.executeUpdate(); //ejecutar query
		
		con.close();  //cierra connection 
		
		out.println("Insertada...! Vuelve atras.");// mensaje de que todo a ido bien
		
	}	
	
}
catch(Exception e)
{
	out.println(e);
}

%>

<html>

	<head>
	
		<title>Aadir registro</title>
		
		<!--css for div main class and table -->
		<style type="text/css">
		
		.main
		{
			width:700px;;
			margin-left:250px;
			padding: 10px;
			border: 5px solid grey;
			
		}
		table
		{
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 600px;
		}
		td
		{
			border: 5px solid silver;
			text-align: left;
			padding: 8px;
		}
		</style>
		
		<!-- javascript for form validation-->
		<script>
		
			function validate()
			{
				var categoria = document.myform.txt_categoria;
				
				if (categoria.value == "")
				{
					window.alert("Por favor introduzca categoria ?");
					categoria.focus();
					return false;
				}
			}
			
		</script>
		
	</head>
	
	<body>

		<div class="main">

		<form method="post" name="myform"  onsubmit="return validate();">
	
			<center>
				<h1>Inserta registro</h1>
			</center>
		
			
			<table>	
		
				<tr>
					<td>Categoria</td>
					<td><input type="text" name="txt_categoria"></td>
				</tr>	
				
				<tr>
					<td><input type="submit" name="btn_add" value="Insert"></td>	
				</tr>
				
			</table>
			
				<center>
					<h1><a href="categorias.jsp">Atras</a></h1>
				</center>
				
		</form>
		
		</div>
		

</body>

</html>