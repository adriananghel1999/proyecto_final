<%@ page import="java.sql.*" %>

<%
try
{
	Class.forName("com.mysql.jdbc.Driver");  //load driver 
	
	Connection con=DriverManager.getConnection("jdbc:mysql://url","user","pass"); // crea connection 
	
	if(request.getParameter("btn_update")!=null) //comprobar que el boton no es null
	{
		int hide; 
		
		String categoria;
		
		hide=Integer.parseInt(request.getParameter("txt_hide")); //esto es la ID
		categoria=request.getParameter("txt_categoria");  //categoria
		
		PreparedStatement pstmt=null; //crea statement  
		
		pstmt=con.prepareStatement("update categorias set categoria=? where id_categoria=?"); //sql update query 
		pstmt.setString(1,categoria);
		pstmt.setInt(2,hide);
		pstmt.executeUpdate(); //ejecuta query
		
		con.close(); //connection cerrada

		out.println("Modificacion completada...! Ve atras."); //Despues de que vaya bien
	}	
	
}
catch(Exception e)
{
	out.println(e);
}

%>

<html>

	<head>
	
		<title>Insertar</title>
		
	<!--css for div main class and table-->
	<style type="text/css">
		
		.main
		{
			width:700px;;
			margin-left:250px;
			padding: 10px;
			border: 5px solid grey;
			
		}
		table
		{
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 600px;
		}
		td
		{
			border: 5px solid silver;
			text-align: left;
			padding: 8px;
		}
		</style>	
		
		<!-- javascript for form validation-->
		<script>
		
			function validate()
			{
				var categoria = document.myform.txt_categoria;
				
				if (categoria.value == "")
				{
					window.alert("Por favor introduce una categoria ?");
					categoria.focus();
					return false;
				}
			}
			
		</script>
		
	</head>
	
<body>

	<div class="main">

	<form method="post" name="myform"  onsubmit="return validate();">
	
		<center>
			<h1>Actualiza el registro</h1>
		</center>
		
		<table>	
		   <%
		try
		{
			Class.forName("com.mysql.jdbc.Driver"); //carga driver  
		
			Connection con=DriverManager.getConnection("jdbc:mysql://ruman.com.es/aga_proyecto","ruman","danidani1"); // crea connection  
	
			if(request.getParameter("edit")!=null) 
			{
				int id=Integer.parseInt(request.getParameter("edit"));
		
				String categoria;
		
				PreparedStatement pstmt=null; // crea statement
				
				pstmt=con.prepareStatement("select * from categorias where id_categoria=?"); // sql select query
				pstmt.setInt(1,id);
				ResultSet rs=pstmt.executeQuery(); // ejecuta query y lo pone en el resultset rs.  
				
				while(rs.next()) 
				{
					id=rs.getInt(1);
					categoria=rs.getString(2);
			%>
			<tr>
				<td>Categoria</td>
				<td><input type="text" name="txt_categoria" value="<%=categoria%>"></td>
			</tr>	
			
			<tr>
				<td><input type="submit" name="btn_update" value="Update"></td>	
			</tr>
				
				<input type="hidden" name="txt_hide" value="<%=id%>">
		<%
				}
			}
		}
		catch(Exception e)
		{
			out.println(e);
		}
		%>	
		</table>
		
		<center>
				<h1><a href="categorias.jsp">Volver</a></h1>
		</center>
		
	</form>

	</div>

</body>

</html>