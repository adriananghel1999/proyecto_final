   <%@page import="java.sql.*" %>

<%
try
{	
	Class.forName("com.mysql.jdbc.Driver");  //driver 
	
	Connection con=DriverManager.getConnection("jdbc:mysql://url","user","pass");  //connection 

	if(request.getParameter("delete")!=null)
	{
		int id=Integer.parseInt(request.getParameter("delete"));
		
		PreparedStatement pstmt=null; //statement
		
		pstmt=con.prepareStatement("delete from categorias where id_categoria=? "); //delete sql query
		pstmt.setInt(1,id);
		pstmt.executeUpdate(); //ejecutar query
		
		con.close(); //cierra connection
	}
}
catch(Exception e)
{
	out.println(e);
}
%>	
<html>

	<head>
	
		<title>Categorias</title>
		
		<!--css for div main class and table -->
		<style type="text/css">
		
		.main
		{
			width:700px;;
			margin-left:250px;
			padding: 10px;
			border: 5px solid grey;
			
		}
		table
		{
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 600px;
		}
		td
		{
			border: 5px solid silver;
			text-align: center;
			padding: 8px;
		}
		</style>
		
		
	</head>	
	
<body>
		<div class="main">
		
			<center>
				<h1><a href="addCategorias.jsp">Aadir fila</a></h1>	
			</center>
		
		<table>
		
			<tr>
				<th>id_categoria</th>
				<th>Categoria</th>
				<th>Modificar</th>
				<th>Borrar</th>
			</tr>
		<%
		
		try
		{	
			Class.forName("com.mysql.jdbc.Driver");  //carga driver 
			
			Connection con=DriverManager.getConnection("jdbc:mysql://ruman.com.es/aga_proyecto","ruman","danidani1");  //crea connection 

			PreparedStatement pstmt=null; //crea statement
		
			pstmt=con.prepareStatement("select * from categorias"); //sql select query  
			
			ResultSet rs=pstmt.executeQuery(); //ejecuta query y lo pone en el resultset rs.  
		
			while(rs.next())
			{	
		%>
				<tr>
					<td><%=rs.getInt(1)%></td>
					<td><%=rs.getString(2)%></td>
					
					<td><a href="updateCategorias.jsp?edit=<%=rs.getInt(1)%> ">Modificar</a></td>
					<td><a href="?delete=<%=rs.getInt(1)%> ">Borrar</a></td>
					
				</tr>
		<%
			}
			
		}
		catch(Exception e)
		{
			out.println(e);
		}
		
		%>
		
		</table>
		
		<center>
				<h1><a href="index.jsp">Volver</a></h1>
		</center>
		
		</div>
		
		
</body>

</html>