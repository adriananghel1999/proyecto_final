<html>
<head>
    
    <style>

        table {
            
            position: absolute;
        }
    
    </style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Acceso a MySQL utilizando JDBC, Tomcat y JSP</title>
</head>
<body>
<%@ page import="java.sql.*" %>
<%!
Connection conConexion;
Statement scSQL;
ResultSet rsListaRegistros;
ResultSetMetaData lsDatos;
%>
<%
Class.forName("com.mysql.jdbc.Driver").newInstance();
conConexion = DriverManager.getConnection("jdbc:mysql://url","user","pass");
scSQL = conConexion.createStatement();
rsListaRegistros = scSQL.executeQuery("SELECT * FROM articulos");
lsDatos = rsListaRegistros.getMetaData();
%>

<br>
<a href="index.jsp">Volver</a>
<br>

<table width="80%" border="1">
<tr>
<% for(int iCont = 1; iCont <= lsDatos.getColumnCount(); iCont++ ) { %>
<th>
<%-- Nombres de las columnas de la tabla MySQL --%>
<%= lsDatos.getColumnLabel(iCont) %>
</th>
<% }%>
</tr>
<% while(rsListaRegistros.next()) { %>
<tr>
<% for(int iCont=1; iCont <= lsDatos.getColumnCount(); iCont++ ) { %>
<%-- Obtenemos el valor de los registros --%>
<td>
<%= rsListaRegistros.getString(iCont) %>
</td>
<% } %>
</tr>
<%}%>

</table>
</body></html>